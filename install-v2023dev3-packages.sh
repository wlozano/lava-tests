#!/bin/sh

grep -q v2023dev3 /etc/apt/sources.list
if [ $? -ne 0 ]
then
    echo "deb https://repositories.apertis.org/apertis/ v2023dev3 target" >> /etc/apt/sources.list
    apt update
    apt install -y linux-image-5.16.0-0.bpo.4-armmp
fi