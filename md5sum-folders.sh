#!/bin/sh
for i in `find /var` ; do
    if ! [ -f $i ] || ! [ -r $i ]
    then
        continue
    fi
    cat $i>/dev/null
done
for i in `find /home /usr` ; do
    if [ $i = "/home/user/.cache/gstreamer-1.0/registry.armv8l.bin" ]
    then
        continue
    fi
    if [ $i = "/home/user/.local/state/wireplumber/restore-stream" ]
    then
        continue
    fi
    if ! [ -f $i ] || ! [ -r $i ]
    then
        continue
    fi
    md5sum $i>>/tmp/md5sum
done
sort -k 2 /tmp/md5sum > /tmp/md5sum-sort
echo md5sum dump start
cat /tmp/md5sum-sort
md5sum /tmp/md5sum-sort
echo md5sum dump stop
