#!/bin/sh

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
echo Test dir $TESTDIR
PREFIX=/nonexistent
case `uname -m` in
x86_64) PREFIX=${TESTDIR}/amd64; ;;
armv7l) PREFIX=${TESTDIR}/armhf; ;;
aarch64) PREFIX=${TESTDIR}/arm64; ;;
esac

echo Runing test
#DEV=`find /dev/disk/by-id | grep mmc-SD | grep -v part`
#echo Using device $DEV
#$PREFIX/mmc extcsd read $DEV || true

dmesg | grep mmc

MMC="mmc0 mmc1 mmc2 mmc3"
for m in $MMC
do
    echo /sys/class/mmc_host/$m
    if ! [ -d /sys/class/mmc_host/$m ]
    then
        continue
    fi
    for d in `ls -d  /sys/class/mmc_host/$m/mmc*`
    do
        echo Info for $m
        FILES="cid manfid oemid name csd serial date"
        for f in $FILES
        do
            echo $f `cat $d/$f`
        done
    done
done
