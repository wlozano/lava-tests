#!/bin/sh

grep -q v2021 /etc/apt/sources.list
if [ $? -ne 0 ]
then
    echo "deb https://repositories.apertis.org/apertis/ v2021 target" >> /etc/apt/sources.list
    apt update
    apt install -y --allow-downgrades util-linux=2.33.1-0.1co2bv2021.0b6 linux-image-5.10.0-14-armmp
    DEBIAN_FRONTEND=noninteractive apt remove -y linux-image-5.16.0-0.bpo.4-armmp
fi