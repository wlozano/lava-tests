#!/bin/sh

DEV=/dev/null
if [ "$1" = "sd" ] ; then  DEV=`find /dev/disk/by-id | grep mmc-SD | grep -v part` fi
echo Using device $DEV
tr '\0' '\377' < /dev/zero > $DEV