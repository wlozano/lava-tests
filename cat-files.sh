#!/bin/sh
exit_code=0
for i in `find /home /usr` ; do
    if ! [ -f $i ] || ! [ -r $i ]
    then
        continue
    fi
    cat $i>/dev/null
    if [ $? -ne 0 ]
    then
        echo Fail to read $i
        journalctl -n 100 --no-pager
        echo Retrying to read $i
        cat $i>/dev/null
        if [ $? -ne 0 ]
        then
            echo Fail to read $i again
        fi
        exit_code=1
    fi
done
journalctl -n 100 --no-pager
exit $exit_code