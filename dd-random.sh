TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
echo Test dir $TESTDIR
PREFIX=/nonexistent
case `uname -m` in
x86_64) PREFIX=${TESTDIR}/amd64; ;;
armv7l) PREFIX=${TESTDIR}/armhf; ;;
aarch64) PREFIX=${TESTDIR}/arm64; ;;
esac

echo Runing test
DEV=/dev/null
echo ARG $1
if [ "$1" = "sd" ] ; then
    DEV=`find /dev/disk/by-id | grep mmc-SD | grep -v part`
fi
echo Using device $DEV
for ((i=1; i<1000; i++)); do
    SEEK=$((1 + $RANDOM % 100000))
    echo Using seek $SEEK
    $PREFIX/dd if=/dev/random of=$DEV bs=4096 seek=$SEEK count=1000 conv=fsync 
done
