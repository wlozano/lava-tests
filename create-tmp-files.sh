#!/bin/sh

cd /home/user
busybox wget -O ospack.tgz https://images.apertis.org/release/v2023dev2/v2023dev2.0rc1/armhf/fixedfunction/ospack_v2023dev2-armhf-fixedfunction_v2023dev2.0rc1.tar.gz
for run in $(seq 100); do DEST=`cat /proc/sys/kernel/random/uuid | sed 's/[-]//g' | head -c 20; echo .tmp`; echo Copying to $DEST; cp ospack.tgz $DEST; done
 