#!/bin/sh

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
echo Test dir $TESTDIR
PREFIX=/nonexistent
case `uname -m` in
x86_64) PREFIX=${TESTDIR}/amd64; ;;
armv7l) PREFIX=${TESTDIR}/armhf; ;;
aarch64) PREFIX=${TESTDIR}/arm64; ;;
esac

if [ "$1" != "" ] ; then
    echo Executing "$1 $2 $3"
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PREFIX
    $PREFIX/bash -c "$1 $2 $3"
fi
