#!/bin/sh

flag=/home/user/fstrim-tweaked
if ! [ -f $flag ]
then
    echo Tweaking fstrim.timer
    cp fstrim.timer /usr/lib/systemd/system
    systemctl daemon-reload
    systemctl enable fstrim.timer
    systemctl start fstrim.timer
    touch $flag
else
    echo fstrim.timer already tweaked
fi
systemctl list-timers