#!/bin/sh

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
echo Test dir $TESTDIR
PREFIX=/nonexistent
case `uname -m` in
x86_64) PREFIX=${TESTDIR}/amd64; ;;
armv7l) PREFIX=${TESTDIR}/armhf; ;;
aarch64) PREFIX=${TESTDIR}/arm64; ;;
esac

echo Runing test
DEV=/dev/null
if [ "$1" = "sd" ] ; then
    DEV=`find /dev/disk/by-id | grep mmc-SD | grep -v part`
fi
echo Using device $DEV
$PREFIX/dd if=/dev/random of=$DEV bs=4096 count=3600000 &
PID_DD=$!
busybox ps
echo Checking PID $PID_DD
while kill -10 $PID_DD ; do sleep 30 ; done
